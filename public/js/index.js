"use strict";
// var ctx = document.getElementById("myChart").getContext('2d');
// var myChart = new Chart(ctx, {
//     type: 'line',
//     data: {
//         labels: ["January", "February", "March", "April", "May", "June", "July", "August"],
//         datasets: [{
//                 label: 'Sales',
//                 data: [3200, 1800, 4305, 3022, 6310, 5120, 5880, 6154],
//                 borderWidth: 2,
//                 backgroundColor: 'rgba(63,82,227,.8)',
//                 borderWidth: 0,
//                 borderColor: 'transparent',
//                 pointBorderWidth: 0,
//                 pointRadius: 3.5,
//                 pointBackgroundColor: 'transparent',
//                 pointHoverBackgroundColor: 'rgba(63,82,227,.8)',
//             },
//             {
//                 label: 'Budget',
//                 data: [2207, 3403, 2200, 5025, 2302, 4208, 3880, 4880],
//                 borderWidth: 2,
//                 backgroundColor: 'rgba(254,86,83,.7)',
//                 borderWidth: 0,
//                 borderColor: 'transparent',
//                 pointBorderWidth: 0,
//                 pointRadius: 3.5,
//                 pointBackgroundColor: 'transparent',
//                 pointHoverBackgroundColor: 'rgba(254,86,83,.8)',
//             }
//         ]
//     },
//     options: {
//         legend: {
//             display: false
//         },
//         scales: {
//             yAxes: [{
//                 gridLines: {
//                     // display: false,
//                     drawBorder: false,
//                     color: '#f2f2f2',
//                 },
//                 ticks: {
//                     beginAtZero: true,
//                     stepSize: 1500,
//                     callback: function (value, index, values) {
//                         return '$' + value;
//                     }
//                 }
//             }],
//             xAxes: [{
//                 gridLines: {
//                     display: false,
//                     tickMarkLength: 15,
//                 }
//             }]
//         },
//     }
// });
