<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'no_ktp', 'ktp_photo', 'role_id', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return Auth::user()->role_id == 1;
    }

    public function isWorker()
    {
        return Auth::user()->role_id == 2;
    }

    public function isUser()
    {
        return Auth::user()->role_id == 3;
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function scopeAs($query, $value)
    {
        return $query->where('role_id', $value);
    }

    public function scopeWorker($query)
    {
        return $query->where('role_id', 2);
    }

    public function scopeUser($query)
    {
        return $query->where('role_id', 3);
    }
}
