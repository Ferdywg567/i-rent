<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['users'] = User::as(3)->paginate(5);
        $data['roles'] = 3;
        return view('users.index', $data);
    }

    public function workers()
    {
        $data['users'] = User::as(2)->paginate(5);
        $data['roles'] = 2;
        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function hire($id)
    {
        $user = User::find($id);
        
        $user->role_id = 2;

        if($user->save()){
            return redirect()->route('users.index')->with('success', $user->name . ' telah menjadi pegawai!');
        }

        return redirect()->back()->with('fail', 'Gagal memperkerjakan!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profile(User $user)
    {
        $data['user'] = $user;

        return view('users.profile', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function verif(User $user, $type)
    {
        $user->ktp_verification = $type;
        if ($user->save()) {
            return redirect()->route('users.index')->with('success', 'KTP '. $user->name . ' telah diverifikasi!');
        }
        return redirect()->back()->with('fail', 'Gagal memverifikasi!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
