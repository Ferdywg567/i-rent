<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DateTime;
use App\Transaction;
use App\Detail;
use App\Car;
use App\Borrow;

class TransactionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(!Auth::user()->isUser()){
            $data['transactions'] = Transaction::paginate(5);
        } else {
            $data['transactions'] = Transaction::where('id_user', Auth::user()->id)->paginate(5);    
        }

        return view('transactions.index', $data);
    }

    public function borrowed($type)
    {
        
        if($type == 'recent'){
            $status = "belum diambil";
        } else {
            $status = "sudah diambil";
        }

        $data['transactions'] = Transaction::where('user_id', Auth::user()->id)->where('status', $status)->paginate(5);
        // $data['transactions'] = Transaction::whereRaw('user_id = ' . Auth::user()->id . ' and status = \''. $status . '')->paginate(5);

        return view('transactions.borrowed', $data);
    }
    public function borrow($id)
    {
        $data['car'] = Car::find($id);

        return view('transactions.borrow', $data);
    }

    public function borrowing(Request $request)
    {
        $prev_trans = 

        $date_first = new DateTime($request->date_first);
        $date_last = new DateTime($request->date_last);
        
        $car_id = $request->car_id;
        $user_id = Auth::user()->id;

        $price = (int) str_replace(',','',$request->price);
        $status = "belum diambil";

        if ($date_first > $date_last) {
            return redirect()->back()->with('fail', 'Tanggal Awal Lebih Besar!');
        }

        $length = $date_first->diff($date_last)->days;
        $total = (($length > 0)? $length : 1) * $price;
        // dd($total);

        $trans = new Transaction;
        $trans->date_first = $date_first->format('Y-m-d');
        $trans->date_last = $date_last->format('Y-m-d');
        $trans->user_id = $user_id;
        $trans->car_id = $car_id;
        $trans->status = $status;
        $trans->total_price = $total;

        if($trans->save()){
            return redirect()->route('cars.index')->with('success', 'Mobil Berhasil Dipinjam!');
        }

        return redirect()->back()->with('fail', 'Mobil Gagal Dipinjam!');
    }

    public function invoice(Transaction $trans)
    {
        $data['trans'] = $trans;
        $first = new DateTime($trans->date_first);
        $last = new DateTime($trans->date_last);
        $period = $first->diff($last);
        $duration = $period->m . ' months, ' . $period->days . ' days, ' . $period->h . ' hours.';
        $data['duration'] = $duration;

        return view('transactions.invoice', $data);
    }

    public function accept(Transaction $trans)
    {
        $trans->status = "sudah diambil";
        if ($trans->save()) {
            return redirect()->route('trans.index')->with('success', 'Transaksi Telah Selesai!');
        }
        return redirect()->back()->with('fail', 'Transaksi Gagal Diselesaikan!');
    }

    public function destroy(Transaction $trans)
    {
        if ($trans->delete) {
            return redirect()->route('trans.index')->with('success', 'Transaksi Berhasil Digagalkan!');
        }

        return redirect()->back()->with('fail', 'Transaksi Gagal Dihapus!');
    }
}
