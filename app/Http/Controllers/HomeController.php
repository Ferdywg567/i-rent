<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Car;
use App\Transaction;
use App\Brand;
use Auth;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index()
    {
        if (!Auth::user()->isUser()) {
            $sum = Transaction::select(DB::raw('sum(total_price) as sum, MONTH(date_first) as month'))->groupBy('month')->orderBy('month', 'asc')->get();
            $all_month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            $months = [];
            foreach($sum as $date) {
                array_push($months, $all_month[$date->month-1]);
            }
            if (Auth::user()->isAdmin()) {                
                $best = Transaction::select(DB::raw('car_id, count(car_id) as total'))->groupBy('car_id')->orderBy('total', 'desc')->take(5)->get();
                $data['bests'] = $best;
            }
            $data['stats'] = $sum;
            $data['months'] = $months;
        }
        
        // dd($best);
        
        $data['workers'] = count(User::where('role_id', 2)->get());
        $data['brands'] = count(Brand::all());
        $data['transactions'] = count(Transaction::all());
        $data['cars'] = Car::latest()->get();
        
        // dd($data);
        
        return view('index', $data);
    }
    
    public function profile(User $user)
    {
        $data['user'] = $user;
        return view('index', $data);
    }
    
    public function changeProfile(User $user)
    {
        $data['user'] = $user;
        return view('index', $data);
    }
    
    public function updateProfile(Request $request)
    {
        return redirect()->route('profile.index');
    }
    
    public function config()
    {
        return view('config');
    }

    public function readNotification($id)
    {
        return redirect()->route('profile.index');
    }
}
