<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Brand;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['brands'] = Brand::All();
        // dd($data['cars'][0]);
        return view('cars.brands', $data);
    }

    public function add()
    {
        return view('cars.addbrand');
    }

    public function create(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required'
        ]);
            
        if ($request->has('name') && $request->image->isValid()) {
            // dd($request->image->extension());
            $name = $request->name;
            $img = $request->image;
            $fileName = time() . '.' . $img->extension();
            $img->move(public_path('image'), $fileName);
            // $img->storeAs('image', $fileName);

            $brand = new Brand;
            $brand->name = $name;
            $brand->photo = $fileName;

            if($brand->save()){
                return redirect()->route('cars.index')->with('success', 'Brand Berhasil Ditambahkan!');
            }
        }
        return redirect()->route('cars.index')->with('fail', 'Brand Gagal Ditambahkan!');
    }

    public function destroy(Brand $brand)
    {
        if($brand->delete()){
            return redirect()->back()->with('success', 'Brand Berhasil Dihapus!');
        }
    }
}
