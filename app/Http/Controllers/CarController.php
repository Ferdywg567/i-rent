<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Car;
use App\Brand;
use App\Transaction;
use Auth;

class CarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['cars'] = Car::paginate(5);
        // dd($data['cars'][0]);
        return view('cars.index', $data);
    }
    public function add()
    {
        $data['brands'] = Brand::All();
        return view('cars.addcar', $data);
    }
    public function create(Request $request)
    {
        // dd($request);
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ]);

        $name = $request->name;
        $brand = $request->brand;
        $plate = $request->plate;
        $img = $request->image;
        $price = (int) str_replace(',','',$request->price);
        $desc = $request->desc;

        $car = new Car;
        $car->name = $name;
        $car->id_brand = $brand;
        $car->price = $price;  
        $car->image = $fileName;
        $car->plate = $plate;
        $car->status = 'Tersedia';
        $car->desc = $desc;

        if($car->save()){

            $img->move(public_path('image'), $fileName);

            return redirect()->route('cars.index')->with('success', 'Mobil Berhasil Dimasukkan!');
        }
        
        return redirect()->back()->with('fail', 'Mobil Gagal Dimasukkan!');

        
    }
    public function edit(Car $car)
    {
        // dd($car);
        $data['brands'] = Brand::All();
        $data['car'] = $car;

        return view('cars.editcar', $data);
    }
    public function update(Request $request)
    {

        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ]);

        $car = Car::find($id);
        if ($car) {

            $id = $request->id;
            $name = $request->name;
            $brand = $request->brand;
            $price = (int) str_replace(',','',$request->price);
            $desc = $request->desc;

            $car->name = $name;
            $car->brand = $brand;
            $car->price = $price;
            $car->desc = $desc;
            if($request->hasFile('image') && $request->image->isValid()){
                $img = $request->image;
                $fileName = time() . '.' . $img->extension();
                $img->move(public_path('image'), $fileName);
                $car->photo = $fileName;
            }
            if($car->save()){
                
                return redirect()->route('cars.index')->with('success', 'Mobil Berhasil Dimasukkan!');
                // return redirect()->back()->with('success', 'Mobil Berhasil Dimasukkan!');
            }
        }
        return redirect()->back()->with('fail', 'Mobil Gagal Dimasukkan!');

        
    }

    public function destroy(Car $car)
    {
        // dd($car);
        if($car->delete()){
            return redirect()->route('cars.index')->with('success', 'Mobil Berhasil Dihapus!');
        }
    }

    public function search(Request $request)
    {
        // dd($request);
        // $brand = $request->id_brand;
        $car = Car::query();
        // dd($car);
        // $car->where('id_brand', $brand);
        if($request->has('id_brand')){
            $brand = $request->id_brand;
            $car->where('id_brand', $brand);
        }

        if($request->has('q')){
            $query = $request->q;
            $car->where('name', 'LIKE', '%'.$query.'%')
            // ->orWhere('id_brand', $query)
            ->orWhere('price', 'LIKE', '%'.$query.'%')
            ->orWhere('desc', 'LIKE', '%'.$query.'%');
        }
        $result = $car->paginate(5);
        // dd($request->q);
        if(count($result) > 0){
            $data['cars'] = $result;
            return view('cars.index', $data);
        } else {    
            return redirect()->route('cars.index')->with('fail', 'Data Tidak Ditemukan!');
        }
    }

    public function detail(Car $car, $notif_id = null)
    {

        if($notif_id){
            Auth::user()->notifications->where('id', $notif_id)->markAsRead();
        }

        $data['car'] = $car;
        $data['borrowed'] = count(Transaction::where('car_id', $car->id)->get());
        return view('cars.detail', $data);
    }
    public function transactions(Request $request)
    {
        $borrow = new Borrow;
        $borrow->date_first = $request->date_first;
        $borrow->car_id = $request->car;
        $data['car'] = $car;

        return view('transactions.transaction', $data);
    }
}
