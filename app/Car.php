<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function brand()
    {
        return $this->hasOne(Brand::class, 'id', 'id_brand');
    }
    public function getPrice()
    {
        return "Rp." . number_format($this->price);
    }
    
}
