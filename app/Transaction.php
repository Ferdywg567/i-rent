<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function getPrice()
    {
        return "Rp." . number_format($this->total_price);
    }
}
