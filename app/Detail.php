<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'id', 'id_transaction');
    }

    public function car()
    {
        return $this->hasOne(Car::class, 'id', 'id_car');
    }
}
