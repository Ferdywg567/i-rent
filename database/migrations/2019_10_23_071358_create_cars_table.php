<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_brand');
            $table->string('name');
            $table->string('desc');
            $table->string('image');
            $table->string('plate');
            $table->unsignedBigInteger('status_id');
            $table->string('price');
            $table->timestamps();

            // $table->foreign('status_id')->references('id')->on('car_statuses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('cars', function (Blueprint $table) {
        //     $table->dropForeign('cars_status_id_foreign');
        // });

        Schema::dropIfExists('cars');
    }
}
