<?php

use Illuminate\Database\Seeder;

use App\Role;
use Faker\Factory;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'name' => 'Admin'
            ],
            [
                'name' => 'Worker'
            ],
            [
                'name' => 'User'
            ],
        ];
        foreach($datas as $data){
            Role::create($data);
        }
    }
}
