<?php

use Faker\Factory;

use Illuminate\Database\Seeder;

use App\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');
        $brands = [
            [
                'name' => 'bmw',
                'photo' => 'bmw.png'
            ],
            [
                'name' => 'honda',
                'photo' => 'honda.png'
            ],
            [
                'name' => 'lamborghini',
                'photo' => 'lamborghini.png'
            ],
            [
                'name' => 'mercedes',
                'photo' => 'mercedes.png'
            ],
            [
                'name' => 'nissan',
                'photo' => 'nissan.png'
            ],
            [
                'name' => 'porsche',
                'photo' => 'porsche.png'
            ],
            [
                'name' => 'tesla',
                'photo' => 'tesla.png'
            ],
            [
                'name' => 'toyota',
                'photo' => 'toyota.png'
            ],
            [
                'name' => 'viper',
                'photo' => 'viper.png'
            ]
        ];
        foreach ($brands as $data) {
            Brand::create($data);
        }
    }
}
