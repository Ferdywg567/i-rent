<?php

use Illuminate\Database\Seeder;

use App\Type;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'contract-name' => 'harian',
                'contract-time' => 1
            ],
            [
                'contract-name' => 'bulanan',
                'contract-time' => 30
            ],
        ];
        foreach ($datas as $data) {
            Type::create($data);
        }
    }
}
