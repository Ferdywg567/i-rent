<?php

use Faker\Factory;

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');
        $datas = [
            [
                'name' => 'Ferdywg567',
                'bio' => $faker->text,
                'address' => $faker->address,
                'photo' => 'avatar-1.png',
                'email' => 'b@b',
                'role_id' => '1',
                'no_ktp' => $faker->numberBetween($min = 3000000000000000, $max = 4000000000000000),
                'password' => bcrypt('12345678'),
                'ktp_photo' => 'ferdy.jpg',
                'ktp_verification' => 'yes'
                
            ],
            [
                'name' => 'Bayuputra',
                'bio' => $faker->text,
                'address' => $faker->address,
                'photo' => 'avatar-1.png',
                'email' => 'a@a',
                'role_id' => '2',
                'no_ktp' => $faker->numberBetween($min = 3000000000000000, $max = 4000000000000000),
                'password' => bcrypt('asdasdasd'),
                'ktp_photo' => 'default.jpg',
                'ktp_verification' => 'no'
            ],
            [
                'name' => 'Adji Maulana Djagad',
                'bio' => $faker->text,
                'address' => $faker->address,
                'photo' => 'avatar-1.png',
                'email' => 'c@c',
                'role_id' => '3',
                'no_ktp' => $faker->numberBetween($min = 3000000000000000, $max = 4000000000000000),
                'password' => bcrypt('qweqweqwe'),
                'ktp_photo' => 'default.jpg',
            ],
        ];
        foreach ($datas as $data) {
            User::create($data);
        }
    }
}
