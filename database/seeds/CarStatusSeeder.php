<?php

use Illuminate\Database\Seeder;

use App\CarStatus as Status;

class CarStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            'tersedia', 'dipinjam', 'maintenance'
        ];

        foreach ($$data as $status) {
            Status::create($status);
        }
    }
}
