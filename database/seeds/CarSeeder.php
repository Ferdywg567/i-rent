<?php

use Faker\Factory;

use Illuminate\Database\Seeder;

use App\Car;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Tesla 
        $faker = Factory::create('id_ID');
        $lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et dui elementum, dignissim neque nec, ullamcorper tellus. Donec lobortis vulputate tortor, bibendum placerat ex efficitur ac.';
        
        $prices = [
            '500000','1000000', '1200000'
        ];
        $cars = [
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'Honda Civic Type-R',
                'desc' => $lorem,
                'image' => 'honda_civic.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'Lamborghini Aventador',
                'desc' => $lorem,
                'image' => 'aventador.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'Lancer Evolution',
                'desc' => $lorem,
                'image' => 'lancer.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'Mercedez-Benz A45',
                'desc' => $lorem,
                'image' => 'mercedes_a45.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'Carrera GT',
                'desc' => $lorem,
                'image' => 'carera_gt.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'Viper',
                'desc' => $lorem,
                'image' => 'dodge_viper.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'GR Supra',
                'desc' => $lorem,
                'image' => 'gr_supra.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'Skyline GT-R',
                'desc' => $lorem,
                'image' => 'skyline.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'SLR McLaren',
                'desc' => $lorem,
                'image' => 'slr_mclaren.jpeg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
            [
                'id_brand' => $faker->numberBetween(1,9),
                'name' => 'BMW M8',
                'desc' => $lorem,
                'image' => 'bmw_m8.jpg',
                'status_id' => $faker->numberBetween(1,3),
                'plate' => 'L ' . $faker->numberBetween($min = 1000, $max = 9000) . ucfirst($faker->randomLetter) . ' ' . $faker->randomDigit,
                'price' => $prices[$faker->numberBetween(0,2)],
            ],
        ];
        foreach ($cars as $data) {
            Car::create($data);
        }
    }
}
