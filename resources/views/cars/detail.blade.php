@extends('layouts.master')
@section('meta-content')
Cars - Detail
@endsection
@section('content')
<div class="section-body">
    <h2 class="section-title">Detail Mobil</h2>
    <p class="section-lead">Berisi semua data mobil terkait</p>
    <div class="card">
        <div class="card-header">
            <img class="m-auto" src="{{asset('image') . '/' . $car->image}}" alt="{{$car->name}}" style="max-width: 100%">
        </div>
        <div class="card-body">
            <div class="container text-center mb-5">
                <h4>{{$car->name}}</h4>
                <img src="{{asset('image') . '/' . $car->brand->photo}}" alt="{{$car->brand->name}}" width="50">
            </div>
            <div class="row">
                <div class="col-6 col-md-6 col-sm-12 row">
                    <div class="col-6 col-md-12 col-sm-6">
                        <span style="font-size: 1.2rem">Car's Name : </span><br><br>
                        {{$car->name}}
                    </div>
                    <div class="col-6 col-md-12 col-sm-6">
                        <span style="font-size: 1.2rem">Car's Brand : </span><br><br>
                        {{$car->brand->name}}
                    </div>
                </div>
                <div class="col-6 col-md-6 col-sm-12 text-center">
                    <span style="font-size: 1.2rem">Description : </span><br><br>
                    {{$car->desc}}
                </div>
                <div class="col-12 mt-5 mb-2">
                    <hr>
                </div>
                <div class="col-6 col-md-6 col-sm-12 mt-4 text-center">
                    <span style="font-size: 1.2rem">Plate : </span><br>
                    <h4>{{$car->plate}}</h4>
                </div>
                <div class="col-6 col-md-6 col-sm-12 mt-4 text-center">
                    <span style="font-size: 1.2rem">Price / Day : </span ><br>
                        <h4>{{$car->getPrice()}}</h4>
                    </div>
                    <div class="col-12 mt-4 mb-4 text-center">
                        <span style="font-size: 1.2rem">Borrowed : </span ><br>
                            <h4>{{$borrowed}}</h4>
                        </div>
                        <div class="col-12 mb-2">
                            <hr>
                        </div>
                    </div>
                    * Data Updated At {{$car->updated_at}}.
                </div>
                <div class="card-footer text-md-right text-sm-center">
                    @if ($car->status == 'tersedia')
                    <a href="{{route('cars.borrow', [$car->id])}}" class="btn btn-success btn-action mr-1" data-toggle="tooltip" title="Borrow"><i class="fas fa-car-side"></i> Borrow Now</a>
                    @endif
                    <a href="{{route('cars.edit', [$car->id])}}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i> Edit Data</a>
                </div>
            </div>
        </div>
        @endsection