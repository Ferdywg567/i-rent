@extends('layouts.master')
@section('meta-content')
    Brands - Add
@endsection

@push('css')
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
@endpush

@section('content')
    <div class="section-body">
        <h2 class="section-title">Tambah Merk Mobil</h2>
        <p class="section-lead">Halaman Tambah Merk Mobil</p>
        <div class="card">
            <div class="card-header">
                <div class="card-header-action">
                    <a href="{{ url()->previous() }}" class="btn btn-primary btn-lg"><i class="fas fa-arrow-left"></i>&nbsp; Back</a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('cars.new.brand')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div id="image-preview" class="image-preview mr-auto ml-auto mb-4">
                                <label for="image-upload" id="image-label">Choose File</label>
                                <input type="file" name="image" id="image-upload"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 m-auto">
                            <input type="text" class="form-control text-center" name="name" style="font-size: 3rem">
                        </div>
                    </div>
                    <div class="form-group m-4">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-success"><i class="fas fa-plus"></i>&nbsp; Add Brand</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('js')
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <script src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>
    <script>
        "use strict";

        $.uploadPreview({
        input_field: "#image-upload",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false,                // Default: false
        success_callback: null          // Default: null
        });
    </script>
@endpush