{{-- @php
    dd(old());
@endphp --}}
@extends('layouts.master')
@section('meta-content')
    Cars
@endsection
@section('content')
    <div class="section-body">
        <h2 class="section-title">Halaman Mobil</h2>
        <p class="section-lead">Berisi semua data mobil yang terdata di database</p>
        <div class="card">
            <div class="card-header">
                @if (!Auth::user()->isUser())
                <a href="{{route('cars.add')}}" class="btn btn-warning btn-lg"><i class="fas fa-plus"></i>&nbsp; Tambah</a>
                <h4></h4>
                @else
                <h4>Data Mobil</h4>
                @endif
                <div class="card-header-form">
                    <form action="{{route('cars.search')}}" method="get">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" value="{{old('q', '')}}" placeholder="Search">
                        <div class="input-group-btn">
                        <button type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    @if(count($cars) > 0)
                        <table class="table table-hover">
                            <thead class="table-thead">
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Merk</th>
                                    <th>Harga/hari</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="table-tbody">
                                @foreach ($cars as $car)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$car->name}}</td>
                                        <td>{{ucfirst($car->brand->name)}}</td>
                                        <td>{{$car->getPrice()}}</td>
                                        <td>
                                            <a href="{{route('cars.detail', [$car->id])}}" class="btn btn-secondary btn-action mr-1" data-toggle="tooltip" title="Detail"><i class="fas fa-eye"></i></a>
                                            @if (!Auth::user()->isUser())
                                                <a href="{{route('cars.edit', [$car->id])}}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                                @if (Auth::user()->isWorker())
                                                    <a href="{{route('cars.borrow', [$car->id])}}" class="btn btn-success btn-action mr-1" data-toggle="tooltip" title="Borrow"><i class="fas fa-car-side"></i></a>
                                                @endif
                                                <a href="#" class="btn btn-danger btn-action car-delete" data-toggle="tooltip" data-id="{{$car->id}}" title="Delete"><i class="fas fa-trash"></i></a>
                                            
                                            @else
                                            <a href="{{route('cars.borrow', [$car->id])}}" class="btn btn-success btn-action" data-toggle="tooltip" title="Borrow"><i class="fas fa-car-side"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right pr-3">
                            {{$cars->links()}}
                        </div>
                    @else
                        <div class="col-md-12 text-center mt-5">
                            <h3 class="title">Data Masih Kosong!</h3>
                            <a href="#" class="btn btn-lg btn-success mt-2 mb-5"><span class="fas fa-plus"></span>&nbsp; Tambah</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('.car-delete').each(function (i) {
            const id = $(this).data('id');
            $(this).click(function(e){
                e.preventDefault();
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Proses ini tidak bisa dikembalikan!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#ffa426',
                    cancelButtonColor: '#fc544b',
                    confirmButtonText: 'Ya, Saya Yakin!'
                    }).then((result) => {
                        if (result.value) {
                            window.location = "{{url('/cars')}}" + '/delete/' + id;
                        } else if (result.dismiss === Swal.DismissReason.cancel){
                            Swal.fire(
                            'Hapus Digagalkan!',
                            'Data Mobil Diselamatkan!',
                            'success'
                            );
                        }
                    });
                });
            }); 
    </script>
@endpush