@extends('layouts.master')
@section('meta-content')
    Car's Brands
@endsection
@push('css')
<style>
    .brand-box:hover{
        background-color: whitesmoke;
    }
</style>    
@endpush
@section('content')
<div class="section-body">
    <h2 class="section-title">Halaman Merk</h2>
    <p class="section-lead">List Semua Merk yg terdata di Database</p>
        <div class="row">
            <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-header-action w-100">
                                <a href="{{ url()->previous() }}" class="btn btn-primary btn-lg"><i class="fas fa-arrow-left"></i>&nbsp; Back</a>
                                @if (!Auth::user()->isUser())
                                <a href="{{ route('cars.add.brand') }}" class="btn btn-warning btn-lg float-right"><i class="fas fa-plus"></i>&nbsp; Tambah</a>
                                @endif
                                
                            </div>
                        </div>
                    </div>    
            </div>
            @if (count($brands) > 0)
                @foreach ($brands as $brand)
                <div class="col-md-4 text-center mb-4 brand-box">
                        <div class="card h-100 card-body" style="min-height: 200px">
                            @if (!Auth::user()->isUser())
                            <a class="ml-auto mr-1 brand-delete" style="color: #aaa" href="#" data-id="{{$brand->id}}"><i class="fas fa-times"></i></a>
                            @endif
                            <img src="{{asset('image/' . $brand->photo)}}" alt="{{$brand->name}}" class="d-block w-50 m-auto" style="max-height: 250px">
                            <a href="{{route('cars.search', ['id_brand' => $brand->id])}}" class="btn btn-lg m-4 btn-user " style="font-size: 1em">{{ucfirst($brand->name)}}</a>
                        </div>
                </div>
                @endforeach
            @else
                <div class="col-md-12 text-center mt-5">
                    <h3 class="title">Data Masih Kosong!</h3>
                    <a href="#" class="btn btn-lg btn-success mt-2 mb-5"><span class="fas fa-plus"></span>&nbsp; Tambah</a>
                </div>
            @endif
        </ul>
        </div>
</div>
@endsection

@push('js')
    <script>
        $('.brand-delete').each(function (i) {
            const id = $(this).data('id');
            $(this).click(function(e){
                e.preventDefault();
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Data mobil terkait juga akan ikut hilang!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#ffa426',
                    cancelButtonColor: '#fc544b',
                    confirmButtonText: 'Ya, Saya Yakin!'
                    }).then((result) => {
                        if (result.value) {
                            window.location = "{{url('/cars')}}" + '/brand/delete/' + id;
                        } else if (result.dismiss === Swal.DismissReason.cancel){
                            Swal.fire(
                            'Hapus Digagalkan!',
                            'Anda Tidak Jadi Menghapus Brand',
                            'success'
                            );
                        }
                    });
                });
            }); 
    </script>
@endpush