@extends('layouts.master')
@section('meta-content')
    Cars - Add
@endsection

@push('css')
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
@endpush

@section('content')
    <div class="section-body">
        <h2 class="section-title">Tambah Mobil</h2>
        <p class="section-lead">Halaman Tambah Mobil</p>
        <div class="card">
            <div class="card-header">
                <div class="card-header-action">
                    <a href="{{ url()->previous() }}" class="btn btn-primary btn-lg"><i class="fas fa-arrow-left"></i>&nbsp; Back</a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('cars.new')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Name</label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Brand</label>
                        <div class="col-sm-12 col-md-7">
                            <select name="brand" class="form-control selectric">
                            @foreach ($brands as $brand)
                                <option value="{{$brand->id}}">{{ucfirst($brand->name)}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Plate</label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" name="plate" class="form-control plate">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Image</label>
                        <div class="col-sm-12 col-md-7">
                            <div id="image-preview" class="image-preview">
                                <label for="image-upload" id="image-label">Choose File</label>
                                <input type="file" name="image" id="image-upload"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Currency</label>
                        <div class="input-group col-sm-12 col-md-7">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Rp.
                                </div>
                            </div>
                            <input type="text" name="price" class="form-control currency">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    / day
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                        <div class="col-sm-12 col-md-7">
                            <textarea name="desc" class="summernote-simple"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-10 text-md-right">
                            <button type="submit" class="btn btn-success"><i class="fas fa-plus"></i>&nbsp; Add Car</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('js')
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <script src="{{asset('js/cleave.min.js')}}"></script>
    <script src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>
    <script>
        "use strict";

        $.uploadPreview({
        input_field: "#image-upload",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false,                // Default: false
        success_callback: null          // Default: null
        });
        var cleaveC = new Cleave('.currency', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
            });
        var cleaveP = new Cleave('.plate', {
            blocks: [1, 4, 2],
            uppercase: true,
            delimiter: ' - ',
            delimiterLazyShow: true
        });
    </script>
@endpush