<div class="navbar-bg"></div>

    <nav class="navbar navbar-expand-lg main-navbar">
    <div class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
        <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
        </ul>
        
    </div>
    <ul class="navbar-nav navbar-right">
    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg {{(Auth::user()->unreadNotifications->count() > 0)? 'beep' : ''}}"><i class="far fa-bell"></i></a>
        <div class="dropdown-menu dropdown-list dropdown-menu-right">
            <div class="dropdown-header">Notifications
            <div class="float-right">
                <a href="#">Mark All As Read</a>
            </div>
            </div>
            <div class="dropdown-list-content dropdown-list-icons">
            <a href="#" class="dropdown-item dropdown-item-unread">
                <div class="dropdown-item-icon bg-primary text-white">
                <i class="fas fa-code"></i>
                </div>
                <div class="dropdown-item-desc">
                Template update is available now!
                <div class="time text-primary">2 Min Ago</div>
                </div>
            </a>
            @if (Auth::user()->notifications)
                @foreach (Auth::user()->notifications as $notification)
                    @switch(substr($notification->type, 18))
                        @case('NewCar')
                            <a href="{{route('cars.detail', [$notification->data['car']['id'],$notification->id])}}" class="dropdown-item">
                                <div class="dropdown-item-icon bg-info text-white">
                                <i class="fas fa-car"></i>
                                </div>
                                <div class="dropdown-item-desc w-100">
                                {{$notification->data['car']['name']}} is available now!
                                <div class="time {{($notification->read_at)? : 'slow-beep'}}">{{$notification->created_at->diffForHumans()}}</div>
                                </div>
                            </a>
                            @break
                        @case('NewUser')
                            <a href="" class="dropdown-item">
                                <div class="dropdown-item-icon bg-info text-white">
                                <i class="far fa-user"></i>
                                </div>
                                <div class="dropdown-item-desc">
                                {{$notification->data['user']['name']}} has fully registered as new member!
                                <div class="time {{($notification->read_at)? : 'slow-beep'}}">{{$notification->created_at->diffForHumans()}}</div>
                                </div>
                            </a>
                            @break
                        @default
                            
                    @endswitch
                @endforeach
            @else

            @endif
            
            <a href="#" class="dropdown-item">
                <div class="dropdown-item-icon bg-success text-white">
                <i class="fas fa-check"></i>
                </div>
                <div class="dropdown-item-desc">
                <b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
                <div class="time">12 Hours Ago</div>
                </div>
            </a>
            <a href="#" class="dropdown-item">
                <div class="dropdown-item-icon bg-danger text-white">
                <i class="fas fa-exclamation-triangle"></i>
                </div>
                <div class="dropdown-item-desc">
                Low disk space. Let's clean it!
                <div class="time">17 Hours Ago</div>
                </div>
            </a>
            </div>
            <div class="dropdown-footer text-center">
            <a href="#">View All <i class="fas fa-chevron-right"></i></a>
            </div>
        </div>
        </li>
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
        <div class="d-sm-none d-lg-inline-block">Hi, {{Auth()->User()->name}}</div></a>
        <div class="dropdown-menu dropdown-menu-right">
            <a href="{{route('profile.index', Auth::user()->id)}}" class="dropdown-item has-icon">
            <i class="far fa-user"></i> Profile
            </a>
            @if (Auth::user()->isAdmin())
            <a href="{{route('setting.general')}}" class="dropdown-item has-icon">
                    <i class="fas fa-cog"></i> Settings
                </a>
            @endif
            <div class="dropdown-divider"></div>
            <div class="text-center">
                <a class="dropdown-item has-icon text-danger" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
                </form>
            </div> 
        </div>
        </li>
    </ul>
    </nav>