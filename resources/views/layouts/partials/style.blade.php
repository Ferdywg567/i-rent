@php
$ashver = "#2d4052";
$ash = "#34495e";
$ashdow = "#8ca6bf";
$bluever = "#394eea";
$blue = "#6777ef";
$bluedow = "#acb5f6";
$greenver = "#169d82";
$green = "#1abc9c";
$greendow = "#81d694";
@endphp

<!-- General CSS Files -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('css/Chart.min.css')}}">


<!-- Template CSS -->
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/components.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
@auth
<style>

    a {
        @if (Auth::user()->isAdmin())
            color: {{$ash}};
        @elseif(Auth::user()->isWorker())
            color: {{$green}};
        @endif
    }

    a:hover {
        @if (Auth::user()->isAdmin())
            color: {{$ashver}};
        @elseif(Auth::user()->isWorker())
            color: {{$greenver}};
        @endif
    }

    .card .card-header h4 {
        @if (Auth::user()->isAdmin())
            color: {{$ash}};
        @elseif(Auth::user()->isWorker())
            color: {{$green}};
        @endif
    }

    .bg-admin {
        background-color: {{$ash}} !important;
    }

    .btn-user {
        @if (Auth::user()->isAdmin())
            background-color: {{$ash}} !important;
            box-shadow: 0 2px 6px {{$ashdow}};
        @elseif(Auth::user()->isWorker())
            background-color: {{$green}} !important;
            box-shadow: 0 2px 6px {{$greendow}};
        @else
            background-color: {{$blue}} !important;
            box-shadow: 0 2px 6px {{$bluedow}};
        @endif
        color: #fff !important
    }

    .btn-user:hover {
        @if (Auth::user()->isAdmin())
            background-color: {{$ashver}} !important;
        @elseif(Auth::user()->isWorker())
            background-color: {{$greenver}} !important;
        @else
            background-color: {{$bluever}} !important;
        @endif
        color: #fff !important
    }

    body.sidebar-mini .main-sidebar .sidebar-menu > li.active > a {
        @if (Auth::user()->isAdmin())
            background-color: {{$ash}} !important;
            box-shadow: 0 2px 6px {{$ashdow}};
        @elseif(Auth::user()->isWorker())
            background-color: {{$green}} !important;
            box-shadow: 0 2px 6px {{$greendow}};
        @endif
    }

    .navbar-bg {
        @if (Auth::user()->isAdmin())
            background-color: {{$ash}} !important;
        @elseif(Auth::user()->isWorker())
            background-color: {{$green}} !important;
        @endif
    }

    .main-sidebar .sidebar-menu li.active a{
        @if (Auth::user()->isAdmin())
            color: {{$ash}};
        @elseif(Auth::user()->isWorker())
            color: {{$green}};
        @endif
    }

    .main-sidebar .sidebar-menu li ul.dropdown-menu li.active > a {
        @if (Auth::user()->isAdmin())
            color: {{$ash}};
        @elseif(Auth::user()->isWorker())
            color: {{$green}};
        @endif
    }

    .main-sidebar .sidebar-menu li ul.dropdown-menu li a:hover {
        @if (Auth::user()->isAdmin())
            color: {{$ash}};
        @elseif(Auth::user()->isWorker())
            color: {{$green}};
        @endif
    }

    .section .section-title:before {
        @if (Auth::user()->isAdmin())
            background-color: {{$ash}};
        @elseif(Auth::user()->isWorker())
            background-color: {{$green}};
        @endif
    }

</style>
@endauth