<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; {{date('Y')}} <div class="bullet"></div> Make By <a href="#">Ferdyansyah Nur Rohim</a>
    </div>
    <div class="footer-right">
        0.8.4
    </div>
</footer>