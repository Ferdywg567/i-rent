<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{route('welcome')}}">i-Rent</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{route('welcome')}}">iR</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Dashboard</li>
              <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                  <li class="active"><a class="nav-link" href="{{route('home')}}">Main Dashboard</a></li>
                </ul>
              </li>
              <li class="menu-header">Car</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-car"></i> <span>Cars</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{route('cars.index')}}">All Cars</a></li>
                  @if (!Auth::user()->isUser())
                    <li><a class="nav-link" href="{{route('cars.add')}}">Add New Car</a></li>
                  @endif
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-layer-group"></i> <span>Brands</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{route('cars.brand')}}">All Brands</a></li>
                  @if (!Auth::user()->isUser())
                    <li><a class="nav-link" href="{{route('cars.add.brand')}}">Add New Brand</a></li>
                  @endif
                </ul>
              </li>
              @if (Auth::user()->isAdmin())
                <li class="menu-header">Users</li>
                <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-user"></i> <span>Users</span></a>
                  <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('users.index')}}">All Users</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-user"></i> <span>Workers</span></a>
                  <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('users.workers')}}">All Workers</a></li>
                    @if (!Auth::user()->isUser())
                      <li><a class="nav-link" href="{{route('users.workers.new')}}">Add New Worker</a></li>
                    @endif
                  </ul>
                </li>
              @endif
                @if (!Auth::User()->isAdmin())
                  <li class="menu-header">Transaction</li>
                  <li class="nav-item dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-handshake"></i> <span>Transactions</span></a>
                    <ul class="dropdown-menu">
                      <li><a class="nav-link" href="{{route('trans.borrowed', 'history')}}">History</a></li>
                      @if (Auth::user()->isWorker())
                        <li><a class="nav-link" href="{{route('trans.index')}}">All Transactions</a></li>
                      @endif
                      <li><a class="nav-link" href="{{route('cars.borrowed','recent')}}">Borrowed Car</a></li>
                    </ul>
                  </li>
                @endif
                <li class="menu-header">Settings</li>
                <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown"><i class="fas fa-user"></i> <span>Profile</span></a>
                  <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('users.profile', Auth::user()->id)}}">Profile</a></li>
                    <li><a class="nav-link" href="{{route('profile.change', Auth::user()->id)}}">Change Profile</a></li>
                    {{-- <li><a class="nav-link beep beep-sidebar" href="components-avatar.html">Recent</a></li> --}}
                  </ul>
                </li>
                {{-- <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown"><i class="fas fa-cog"></i> <span>settings</span></a>
                    <ul class="dropdown-menu">
                      <li><a class="nav-link" href="{{route('setting.general')}}">General</a></li>
                      @if (Auth::user()->isAdmin())
                        <li><a class="nav-link" href="{{route('setting.advance')}}">Advance</a></li>
                      @endif
                    </ul>
                </li> --}}
            </ul>
        </aside>
      </div>