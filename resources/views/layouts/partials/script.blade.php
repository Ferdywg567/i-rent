<!-- General JS Scripts -->
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/nicescroll.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('js/stisla.js')}}"></script>
<script src="{{asset('js/Chart.min.js')}}"></script>
<script src="{{asset('js/sweetalert2.all.min.js')}}"></script>

<!-- Template JS File -->
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>

<script>
    $(document).ready(function () {
        var msg;
        var success = '{{Session::has('success')}}';
        var fail = '{{Session::has('fail')}}';
        var error = '{{!$errors->isEmpty()}}';

        if (error){
            Swal.fire({
                type: 'error',
                title: 'Oopss...',
                text: 'Your Input Doesn\'t Valid!'
            });
        }
        
        if(success) {
            msg = '{{Session::get('success')}}';
            Swal.fire({
                type: 'success',
                title: 'Success!',
                text: msg
            });
        } else if(fail) {
            msg = '{{Session::get('fail')}}';
            Swal.fire({
                type: 'error',
                title: 'Sorry...',
                text: msg
            });
        }
    });
</script>
