<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Ecommerce Dashboard &mdash; Stisla</title>
  <style>
    .slow-beep {
    position: relative;
}

.slow-beep:after {
    content: '';
    position: absolute;
    top: 50%;
    right: 8px;
    width: 10px;
    height: 10px;
    background-color: #ffa426;
    border-radius: 50%;
    -webkit-animation: kedip 1.5s ease-out;
    animation: kedip 1.5s ease-out;
    -webkit-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
    opacity: 1;
}
@keyframes kedip {
    0% {
        opacity: 1;
    }

    40% {
        opacity: 1;
    }

    60% {
        opacity: 0;
    }

    80% {
        opacity: 1;
    }

    100% {
        opacity: 1;
    }
}
  </style>
  @include('layouts.partials.style')
  @stack('css')
</head>
<body>
  <div id="app">
    <div class="main-wrapper">
      @include('layouts.partials.navbar')

      @include('layouts.partials.sidebar')
      
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
            <div class="section-header">
              <h1>@yield('meta-content')</h1>
            </div>
            @yield('content')
          </section>
      </div>
      @include('layouts.partials.footer')
    </div>
  </div>
  @yield('modal')
</body>
@include('layouts.partials.script')
@stack('js')
<script>
  $(document).ready(function () {
      var msg;
      var success = '{{Session::has('success')}}';
      var fail = '{{Session::has('fail')}}';
      if(success) {
          msg = '{{Session::get('success')}}';
          Swal.fire({
              type: 'success',
              title: 'Success!',
              text: msg
          });
      } else if(fail) {
          msg = '{{Session::get('fail')}}';
          Swal.fire({
              type: 'error',
              title: 'Sorry...',
              text: msg
          });
      }
  });
</script>
</html>
