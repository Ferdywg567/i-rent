<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
        
        <title>I-Rent</title>

        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> --}}
        <link href="{{ asset('css/nunito-light-bold.css') }}" rel="stylesheet">
        <link href="{{ asset('css/roboto.css') }}" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    </head>
    <body>
        <section class="flex-center position-ref full-height" id="home">
            @if (Route::has('login'))
                <div class="top-right links">
                        @auth
                            <a href="{{ route('home') }}">Dashboard</a>
                            <a class="dropdown-item has-icon text-danger" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                            </form>
                        @else
                            <a href="{{ route('login') }}">Login</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">Register</a>
                            @endif
                        @endauth
                </div>
            @endif

            <div class="content fade-up">
                <div class="title">
                    I-Rent
                </div>
                <div class="subtitle">
                    <hr class="lineup" width="50%">
                    The Best Describe Your Desire
                    <br>
                    <br>
                    <a href="#advantages" class="a-homepage fade-up delay-2">Read More</a>
                </div>
            </div>
        </section>
        <div class="flex-center full-height" style="padding-top: 3rem;padding-bottom: 3rem" id="advantages">
            <div class="container">
                <div class="content title">
                    Apa itu I-Rent?
                    <hr class="lineup" width="50%">
                </div>
                <p class="subtitle">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p class="subtitle">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                
            </div>
        </div>
        <div class="flex-center half-height" id="ask">
            <div class="content title">
                Apa kelebihan nya?
            </div>
        </div>
        <div class="flex-center full-height" id="about">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <span class="fas fa-user fa-2x"></span>
                        <p>Users Friendly</p>
                    </div>
                    <div class="col">
                        <span class="fas fa-car fa-2x"></span>
                        <p>Have a Sport Car</p>
                    </div>
                    <div class="col">
                            <span class="fas fa-smile fa-2x"></span>
                        <p>Have a good Profile</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-center half-height" id="ask">
            <div class="content title">
                Bagaimana Cara kerjanya?
            </div>
        </div>
        <div class="flex-center full-height" id="quote">
            <div class="container-fluid">
                <div class="content title">
                    Tenang...
                </div>
                <p class="content subtitle">
                    Ini Carousel nya,
                    Anggap saja Quote :v
                </p>
                <!-- Slideshow container -->
                <div class="slideshow-container">
                    <!-- Full-width images with number and caption text -->
                    <div class="mySlides fade-right">
                        <div class="container half-height flex-center">
                            <div class="subtitle"><i class="fas fa-quote-left fa-pull-left"></i> Pada Akhirnya, Hasil Lah Yang Akan Menjadi Penilaian <i class="fas fa-quote-right fa-pull-right"></i><br>-Someone</div>
                            
                        </div>
                    </div>
                
                    <div class="mySlides fade-right">
                        <div class="container half-height flex-center">
                            <div class="subtitle"><i class="fas fa-quote-left fa-pull-left"></i> INI ADALAH JALAN NINJAKU! <i class="fas fa-quote-right fa-pull-right"></i><br>-My Friend</div>
                        </div>
                    </div>
                
                    <div class="mySlides fade-right">
                        <div class="container half-height flex-center">
                            <div class="subtitle"><i class="fas fa-quote-left fa-pull-left"></i> Bila Kau Tak Tahan Lelahnya Belajar, Maka Kau Harus Tahan Menanggung Perihnya Kebodohan <i class="fas fa-quote-right fa-pull-right"></i><br>-Imam Syafi'i</div>
                        </div>
                    </div>
                
                    <!-- Next and previous buttons -->
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>
                </div>
                <br>
                
                <!-- The dots/circles -->
                <div style="text-align:center">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                    <span class="dot" onclick="currentSlide(3)"></span>
                </div>
            </div>
        </div>
        <div class="flex-center half-height" id="contact">
            <div class="container content">
                <div class="title">
                    Subscribe Lah!
                </div>
                <form id="contact-form" action="#">
                    <input type="text" placeholder="JohnDoe@email.com">
                    <a href="#" class="btn-form">Subscribe</a>
                </form>
            </div>
        </div>
        <div class="flex-center position-ref footer">
            <div class="subtitle">Copyright &copy; {{date('Y')}}, Ferdyansyah Nur Rohim. Make With <i style="color: #b01212" class="fas fa-heart"></i></div>
        </div>
    </body>
</html>
