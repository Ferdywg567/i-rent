@extends('layouts.master')
@section('meta-content')
    Transactions - Add
@endsection

@push('css')
    {{-- <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}"> --}}
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
@endpush

@section('content')
    <div class="section-body">
        <h2 class="section-title">Pinjam Mobil</h2>
        <p class="section-lead">Halaman Pinjam Mobil</p>
        <div class="card">
            <div class="card-header">
                <div class="card-header-action">
                    <a href="{{ url()->previous() }}" class="btn btn-primary btn-lg"><i class="fas fa-arrow-left"></i>&nbsp; Back</a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('cars.borrowing')}}" method="POST">
                    @csrf
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Tanggal Pinjam</label>
                        <div class="col-sm-12 row col-md-7">
                            <div class="col-md-5 col-sm-12">
                                <input type="text" name="date_first" class="form-control datepicker">
                            </div>                            
                            <div class="col-1 col-lg-1 col-md-2 col-sm-12 pt-1 pb-1 mt-2 mb-2 text-center">To</div>
                            <div class="col-md-5 col-sm-12">
                                <input type="text" name="date_last" class="form-control datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Car</label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" name="car" class="form-control" value="{{$car->name}}" readonly>
                            <input type="text" name="car_id" class="form-control" value="{{$car->id}}" hidden>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 pt-2">Currency</label>
                        <div class="input-group col-sm-12 col-md-7">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Rp.
                                </div>
                            </div>
                            <input type="text" name="price" value="{{$car->price}}" class="form-control currency" readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    / day
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-10 text-md-right">
                            <button type="submit" class="btn btn-success"><i class="fas fa-car-side"></i>&nbsp; Borrow Car</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('js')
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <script src="{{asset('js/cleave.min.js')}}"></script>
    <script>
        "use strict";

        var cleaveC = new Cleave('.currency', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
            });
    </script>
@endpush