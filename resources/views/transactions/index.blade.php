@extends('layouts.master')
@section('meta-content')
Transactions
@endsection
@section('content')
<div class="section-body">
    <h2 class="section-title">Halaman Transaksi</h2>
    @if (Auth::user()->isWorker())
    <p class="section-lead">Berisi semua data transaksi yg terdata di database</p>
    @else
    <p class="section-lead">Berisi semua data transaksi milikmu</p>
    @endif
    <div class="card">
        <div class="card-header">
            <a href="{{route('cars.index')}}" class="btn btn-warning btn-lg"><i class="fas fa-plus"></i>&nbsp; Tambah</a>
            <h4></h4>
            <div class="card-header-form">
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-info"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="table-responsive">
                @if(count($transactions) > 0)
                <table class="table table-hover">
                    <thead class="table-thead">
                        <tr>
                            <th>Id</th>
                            <th>Peminjam</th>
                            <th>Tanggal Kembali</th>
                            <th>Status</th>
                            <th>Total Harga</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="table-tbody">
                        @foreach ($transactions as $trans)
                        <tr>
                            <td>{{$trans->id}}</td>
                            <td>{{$trans->user->name}}</td>
                            <td>{{$trans->date_last}}</td>
                            <td>{{$trans->status}}</td>
                            <td>{{$trans->getPrice()}}</td>
                            <td>
                                <a href="{{route('trans.invoice', [$trans->id])}}" class="btn btn-secondary btn-action mr-1" data-toggle="tooltip" title="detail"><i class="fas fa-eye"></i></a>
                                @if (Auth::user()->isWorker() && $trans->user_id != Auth::user()->id)
                                <a href="{{route('trans.edit', [$trans->id])}}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                @endif
                                @if ($trans->status == 'belum diambil')
                                <a href="{{route('trans.accept', [$trans->id])}}" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Take"><i class="fas fa-hands"></i></a>
                                @endif
                                <a href="{{route('trans.delete', [$trans->id])}}" class="btn btn-danger btn-action btn-delete" data-toggle="tooltip" title="Delete"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="float-right pr-3">
                    {{$transactions->links()}}
                </div>
                @else
                <div class="col-md-12 text-center mt-5">
                    <h3 class="title">Data Masih Kosong!</h3>
                    <a href="{{route('cars.index')}}" class="btn btn-lg btn-success mt-2 mb-5">Pinjam Mobil</a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $('.btn-accept').each(function (i) {
        const id = $(this).data('id');
        // const name = $(this).data('name');
        $(this).click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Anda Yakin?',
                text: "Transaksi tidak dapat dibatalkan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#47c363',
                cancelButtonColor: '#fc544b',
                confirmButtonText: 'Ya, Saya Yakin!'
            }).then((result) => {
                if (result.value) {
                    window.location = "{{url('/transactions/accept')}}" + '/' + id;
                } else if (result.dismiss === Swal.DismissReason.cancel){
                    Swal.fire(
                    'Woopss!',
                    'Transaksi tidak jadi selesai...',
                    'error'
                    );
                }
            });
        });
    });
</script>
@endpush