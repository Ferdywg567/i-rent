@extends('layouts.master')
@section('meta-content')
Invoice
@endsection

@push('css')
<style>
  @media print {
    @page {size: 290mm 230mm;}
    
    body * {
      visibility: hidden;
    }
    #section-to-print, #section-to-print * {
      visibility: visible;
    }
    #section-to-print {
      width: 100%;
      position:absolute;
      top: 0;
      left: 0;
      bottom: 0;
    }
  }
</style>
@endpush
@section('content')
<div class="section-body">
  <div class="invoice">
    <div class="invoice-print" id="section-to-print">
      <div class="row">
        <div class="col-lg-12">
          <div class="invoice-title">
            <h2>Invoice</h2>
            <div class="invoice-number">Order #{{$trans->id}}</div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <address>
                <strong>Shipped To:</strong><br>
                {{$trans->user->name}}<br>
                {{$trans->user->email}}<br>
                {{$trans->user->address}}<br>
              </address>
            </div>
            <div class="col-md-6 text-md-right">
              <address>
                <strong>Order Date:</strong><br>
                {{$trans->created_at}}<br>
                <strong>Payment Method:</strong><br>
                On The Spot<br>
              </address>  
            </div>
          </div>
        </div>
      </div>
      
      <div class="row mt-4">
        <div class="col-md-12">
          <div class="section-title">Order Summary</div>
          <p class="section-lead">All items here cannot be deleted.</p>
          <div class="table-responsive">
            <table class="table table-striped table-hover table-md">
              <tr>
                <th data-width="40">#</th>
                <th>Item</th>
                <th class="text-center">Price</th>
                <th class="text-md-center">Duration</th>
                <th class="text-right">Total</th>
              </tr>
              <tr>
                <td>1</td>
                <td>{{$trans->car->name}}</td>
                <td class="text-center">{{$trans->car->getPrice()}}</td>
                <td class="text-md-center">{{$duration}}</td>
                <td class="text-right">{{$trans->getPrice()}}</td>
              </tr>
            </table>
          </div>
          <div class="row mt-4">
            <div class="col-lg-8">
              
            </div>
            <div class="col-lg-4 text-right">
              <div class="invoice-detail-item">
                <div class="invoice-detail-name">Subtotal</div>
                <div class="invoice-detail-value">{{$trans->getPrice()}}</div>
              </div>
              <div class="invoice-detail-item">
                <div class="invoice-detail-name">Shipping</div>
                <div class="invoice-detail-value">Rp. 0</div>
              </div>
              <hr class="mt-2 mb-2">
              <div class="invoice-detail-item">
                <div class="invoice-detail-name">Total</div>
                <div class="invoice-detail-value invoice-detail-value-lg">{{$trans->getPrice()}}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="text-md-right">
      <div class="float-lg-left mb-lg-0 mb-3">
        <button class="btn btn-primary btn-icon icon-left btn-accept" data-id="{{$trans->id}}"><i class="fas fa-credit-card"></i> Process Payment</button>
        <button class="btn btn-danger btn-icon icon-left btn-cancel" data-id="{{$trans->id}}"><i class="fas fa-times"></i> Cancel</button>
      </div>
      <button class="btn btn-warning btn-icon icon-left" onclick="window.print()"><i class="fas fa-print"></i> Print</button>
    </div>
  </div>
</div>
@endsection
@push('js')
<script>
  $('.btn-accept').each(function (i) {
    const id = $(this).data('id');
    // const name = $(this).data('name');
    $(this).click(function(e){
      e.preventDefault();
      Swal.fire({
        title: 'Anda Yakin?',
        text: "Transaksi tidak dapat dibatalkan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#47c363',
        cancelButtonColor: '#fc544b',
        confirmButtonText: 'Ya, Saya Yakin!'
      }).then((result) => {
        if (result.value) {
          window.location = "{{url('/transactions/accept')}}" + '/' + id;
        } else if (result.dismiss === Swal.DismissReason.cancel){
          Swal.fire(
          'Woopss!',
          'Transaksi tidak jadi selesai...',
          'error'
          );
        }
      });
    });
  });

  $('.btn-cancel').each(function (i) {
    const id = $(this).data('id');
    $(this).click(function(e){
      e.preventDefault();
      Swal.fire({
        title: 'Anda Yakin?',
        text: "Transaksi akan dihapus!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#47c363',
        cancelButtonColor: '#fc544b',
        confirmButtonText: 'Ya, Saya Yakin!'
      }).then((result) => {
        if (result.value) {
          window.location = "{{url('/transactions/accept')}}" + '/' + id;
        } else if (result.dismiss === Swal.DismissReason.cancel){
          Swal.fire(
          'Save~',
          'Data transaksi diselamatkan!',
          'success'
          );
        }
      });
    });
  });
</script>
@endpush