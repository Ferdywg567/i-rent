@extends('layouts.master')
@section('meta-content')
Profile
@endsection
@push('css')
<style>
  .equal {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    flex-wrap: wrap;
  }
  .equal > [class*='col-'] {
    display: flex;
    flex-direction: column;
  }
  #ktp-photo {
    width: 50%;
  }
</style>
@endpush
@section('content')
<div class="section-body">
  <br>
  <br>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="container">
            <div class="row">
              <div class="col-12 text-center mb-4">
                <div class="rounded-circle" style="width: 200px; height: 200px; background: url('{{asset('image') . '/' . $user->photo}}') center no-repeat; background-size: cover; border: 15px solid white; position: absolute; top: -15px; left: 50%; transform: translate(-50%, -50%)"></div>
                <h1 style="margin-top: 6rem !important">{{$user->name}}</h1>
                <h4>{{$user->role->name}}</h4>
              </div>
              <div class="col-12 col-md-6 col-sm-12 bg-primary text-center p-4" style="min-height: 200px; color: white">
                <div class="h-100">
                  <i class="fas fa-home" style="font-size: 40px"></i>
                  <p class="mt-4" style="font-size: 18px">{{$user->address}}</p>
                </div>
              </div>
              <div class="col-12 col-md-6 col-sm-12 bg-success text-center p-4" style="min-height: 200px; color: white">
                <div class="h-100">
                  <i class="fas fa-align-left" style="font-size: 40px"></i>
                  <p class="mt-4" style="font-size: 14px">{{$user->bio}}</p>
                </div>
              </div>
              <div class="col-12 col-md-6 col-sm-12 bg-danger text-center p-4" style="min-height: 200px; color: white">
                <div class="h-100">
                  <i class="fas fa-at" style="font-size: 40px"></i>
                  <p class="mt-4" style="font-size: 18px">{{$user->email}}</p>
                </div>
              </div>
              <div class="col-12 col-md-6 col-sm-12 bg-admin text-center p-4" style="min-height: 200px; color: white">
                <div class="h-100">
                  <i class="far fa-id-card" style="font-size: 40px"></i>
                  <p class="mt-4" style="font-size: 18px">{{$user->no_ktp}}</p>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="text-center">
              <h4>KTP Photo</h4>
            <img id="ktp-photo" src="{{asset('image') .'/'. $user->ktp_photo}}" alt="">
          </div>
        </div>
        <div class="card-footer text-center">
          <a href="{{route('users.edit', $user->id)}}" class="btn btn-primary"><i class="fas fa-edit"></i> Edit Profile</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection