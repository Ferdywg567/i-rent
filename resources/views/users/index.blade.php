@extends('layouts.master')
@section('meta-content')
    Users @if ($roles == 2) - Workers @endif
@endsection
@section('content')
    <div class="section-body">
        <h2 class="section-title">Halaman @if ($roles == 3) Users @else Workers @endif</h2>
        <p class="section-lead">Berisi semua data @if ($roles == 3) user @else worker @endif yg terdata di database</p>
        <div class="card">
            <div class="card-header">
                <h4>Data @if ($roles == 3) Users @else Workers @endif</h4>
                <div class="card-header-form">
                    <form>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                        <button class="btn btn-info"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    @if(count($users) > 0)
                        <table class="table table-hover">
                            <thead class="table-thead">
                                <tr>
                                    <th>No. </th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>KTP Verification</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="table-tbody">
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->ktp_verification}}</td>
                                        <td>
                                            <a href="{{route('users.edit', [$user->id])}}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                            @if ($user->ktp_verification != 'wait' && $user->ktp_verification != 'no')
                                                <a href="#" data-id="{{$user->id}}" data-name="{{$user->name}}" class="btn btn-success btn-action btn-hire" data-toggle="tooltip" title="Hire"><i class="fas fa-hands-helping"></i></a>
                                            @else
                                                <a href="" class="btn btn-warning btn-action btn-verif" data-img="{{$user->ktp_photo}}" data-name="{{$user->name}}" data-id="{{$user->id}}" data-toggle="modal" data-target="#exampleModal" title="Verif"><i class="far fa-id-card"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right pr-3">
                            {{$users->links()}}
                        </div>
                    @else
                        <div class="col-md-12 text-center mt-5">
                            <h3 class="title">Data Masih Kosong!</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New message</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="text-center container-fluid">
                <img src="" alt="image" width="360">
            </div>
        </div>
        <div class="modal-footer justify-content-center">
          <a href="" id="verif-no" class="btn rounded-0 btn-md btn-outline-danger" data-dismiss="modal">Don't Accept</a>
          <span class="m-2">Or</span>
          <a href="" id="verif-yes" class="btn rounded-0 btn-md btn-primary">Accept This</a>
        </div>
      </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Pass ID
        var recipient = button.data('name') // Extract info from data-* attributes
        var img = button.data('img') // for Image
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Verify ' + recipient)
        modal.find('.modal-body img').attr('src', "{{asset('image')}}/" + img)
        modal.find('#verif-no').attr('href', "{{url('/users/verif')}}" + "/" + id + "/" +'no')
        modal.find('#verif-yes').attr('href', "{{url('/users/verif')}}" + "/" + id + "/" +'yes')
        });
        $('.btn-hire').each(function (i) {
            const id = $(this).data('id');
            const name = $(this).data('name');
            $(this).click(function(e){
                e.preventDefault();
                Swal.fire({
                    title: 'Anda Yakin?',
                    text: "Anda yakin ingin memperkerjakan " + name + "?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#47c363',
                    cancelButtonColor: '#fc544b',
                    confirmButtonText: 'Ya, Saya Yakin!'
                    }).then((result) => {
                        if (result.value) {
                            window.location = "{{url('/users/hire')}}" + '/' + id;
                        } else if (result.dismiss === Swal.DismissReason.cancel){
                            Swal.fire(
                            'Gagal Memperkerjakan!',
                            'User tidak jadi pegawai!',
                            'success'
                            );
                        }
                    });
                });
            }); 
    </script>
@endpush