@extends('layouts.master')
@section('meta-content')
    Users - Workers
@endsection
@section('content')
    <div class="section-body">
        <h2 class="section-title">Halaman User Pegawai</h2>
            <p class="section-lead">Berisi semua data Pegawai yg terdata di database</p>
        <div class="card">
                <div class="card-header">
                    <h4>Data Workers</h4>
                    <div class="card-header-form">
                        <form>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                            <button class="btn btn-info"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        @if(count($workers) > 0)
                            <table class="table table-hover">
                                <thead class="table-thead">
                                    <tr>
                                        <th>No. </th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>No. KTP</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="table-tbody">
                                    @foreach ($workers as $worker)
                                        <tr>
                                            <td>{{$worker->id}}</td>
                                            <td>{{$worker->name}}</td>
                                            <td>{{$worker->email}}</td>
                                            <td>{{$worker->no_ktp}}</td>
                                            <td style="width: 15%">
                                                <a href="{{route('workers.detail', [$worker->id])}}" class="btn btn-success btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-eye"></i></a>
                                                @if (!Auth::user()->isUser())
                                                    <a href="{{route('workers.edit', [$worker->id])}}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                                @else
                                                    <a href="{{route('workers.delete', [$worker->id])}}" class="btn btn-danger btn-action btn-delete" data-toggle="tooltip" title="Delete"><i class="fas fa-trash"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="float-right pr-3">
                                {{$workers->links()}}
                            </div>
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <h3 class="title">Data Masih Kosong!</h3>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
    </div>
@endsection
