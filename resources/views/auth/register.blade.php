@extends('layouts.auth')
@section('title')
    Register
@endsection
@section('content')
<div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
        <div class="login-brand">
          <img src="{{asset('image/logo-flat-notext.png')}}" alt="logo" width="100" class="shadow-light rounded-circle">
        </div>

        <div class="card card-primary">
          <div class="card-header"><h4>Register</h4></div>

          <div class="card-body">
            <form action="{{route('register')}}" enctype="multipart/form-data" method="POST">
              @csrf
                <div class="form-group">
                  <label for="name">Name <span class="required">*</span></label>
                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autofocus>
                  @error('name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>

              <div class="form-group">
                <label for="email">Email <span class="required">*</span></label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              
              <div class="form-group">
                <label for="no_ktp">No. KTP <span class="required">*</span></label>
                <input id="no_ktp" type="number" class="form-control @error('no_ktp') is-invalid @enderror" name="no_ktp">
                @error('no_ktp')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="row">
                <div class="form-group col-6">
                  <label for="password" class="d-block">Password <span class="required">*</span></label>
                  <input id="password" type="password" class="form-control pwstrength @error('password') is-invalid @enderror" data-indicator="pwindicator" name="password">
                  <div id="pwindicator" class="pwindicator">
                    <div class="bar"></div>
                    <div class="label"></div>
                  </div>
                  @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
                <div class="form-group col-6">
                  <label for="password_confirm" class="d-block">Password Confirmation <span class="required">*</span></label>
                  <input id="password_confirm" type="password" class="form-control" name="password_confirmation" required>
                  <div class="message"></div>
                </div>
              </div>

              <div class="form-group text-center">
                <label for="image-upload" class="d-block">KTP Photo (required, but can be inputted after registration)</label>
                <div id="image-preview" class="image-preview m-auto" style="width: 75%">
                  <label for="image-upload" id="image-label">Choose File</label>
                  <input type="file" name="image" id="image-upload"/>
                </div>
              </div>

              <div class="form-group">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" name="agree" class="custom-control-input" id="agree">
                  <label class="custom-control-label" for="agree">I agree with the <a href="{{route('terms')}}">terms and conditions</a></label>
                </div>
              </div>

              <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-lg btn-block">
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
        <div class="mt-5 text-muted text-center">
          Already have an account? <a href="{{route('register')}}">Login</a>
        </div>
        <div class="simple-footer">
          Copyright &copy; Ferdyansyah Nur Rohim {{date('Y')}}
        </div>
      </div>
@endsection

@push('css')
    <style>
      .required {
        color: red;
      }
    </style>
@endpush

@push('js')
<script src="{{asset('js/jquery.uploadPreview.min.js')}}"></script>
  <!-- JS Libraies -->
  <script src="{{asset('js/jquery.pwstrength.min.js')}}"></script>
  <!-- Page Specific JS File -->
  <script src="{{asset('js/auth-register.js')}}"></script>
    <script>
        $('#password, #password_confirm').on('keyup', function () {
            if ($('#password').val() == $('#password_confirm').val() && $('#password_confirm').val() != "") {
              $('.message').html('Matching').css('color', '#00DD00');
            } else if($('#password_confirm').val() == "") {
              $('.message').html('');
            } else {
              $('.message').html('Not Matching').css('color', '#DD0000');
            }
        });

        $.uploadPreview({
            input_field: "#image-upload",   // Default: .image-upload
            preview_box: "#image-preview",  // Default: .image-preview
            label_field: "#image-label",    // Default: .image-label
            label_default: "Choose File",   // Default: Choose File
            label_selected: "Change File",  // Default: Change File
            no_label: false,                // Default: false
            success_callback: null          // Default: null
        });
    </script>
    
@endpush