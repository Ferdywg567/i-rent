@extends('layouts.master')
@section('meta-content')
Dashboard
@endsection
@section('content')
@if (!Auth::user()->isUser())
<div class="row">
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-admin">
        <i class="fas fa-users"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>Total Worker</h4>
        </div>
        <div class="card-body">
          @if($workers > 0)
          {{$workers}}
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-danger">
        <i class="fas fa-car"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>Total Cars</h4>
        </div>
        <div class="card-body">
          @isset($cars)
          {{count($cars)}}
          @endisset
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-warning">
        <i class="fas fa-handshake"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>Transactions</h4>
        </div>
        <div class="card-body">
          @if($transactions > 0)
          {{$transactions}}
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-success">
        <i class="fas fa-layer-group"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>All Brands</h4>
        </div>
        <div class="card-body">
          @if($brands)
          {{$brands}}
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@if (!Auth::user()->isUser())
<div class="row">
  <div class="@if(Auth::user()->isAdmin()) col-lg-8 @else col-lg-12 @endif col-md-12 col-12 col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4>Sales Graph</h4>
      </div>
      <div class="card-body">
        @if (count($stats))
          <canvas id="myChart" height="158"></canvas>
        @else
            <div class="text-center" style="height: 315px; line-height:315px">
              <h2 class="d-inline-block" style="vertical-align:middle; line-height: normal;">No Data</h2>
            </div>
        @endif
      </div>
    </div>
  </div>
  @if (Auth::user()->isAdmin())
  <div class="col-lg-4 col-md-12 col-12 col-sm-12">
    <div class="card gradient-bottom" style="height: 93.6%">
      <div class="card-header">
        <h4>Top 5 Cars</h4>
      </div>
      <div class="card-body" id="top-5-scroll">
        @if (count($bests))
          <ul class="list-unstyled list-unstyled-border">
            <li class="media">
              <div class="media-body">
                @foreach ($bests as $best)
                  {{-- <div class="float-right"><div class="font-weight-600 text-muted text-small">{{$best->total}} Sales</div></div> --}}
                  <div class="media-title">{{$best->car->name}}</div>
                  <div class="mt-1">
                    <p>{{$best->total}} Sales</p>
                  </div>
                @endforeach
              </div>
            </li>
          </ul>
        @else
          <div class="text-center" style="height: 315px; line-height:315px">
            <h2 class="d-inline-block" style="vertical-align:middle; line-height: normal;">No Data</h2>
          </div>
        @endif
      </div>
    </div>
  </div>
  @else
  
  @endif
</div>
@endif
<div class="row">
  <div class="col-lg-12 col-md-12 col-12 col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4>Latest Cars</h4>
        <div class="card-header-action">
          <a href="{{route('cars.index')}}" class="btn btn-info"><i class="fas fa-search"></i>&nbsp; View All</a>
        </div>
      </div>
      <div class="card-body p-0">
        <div class="table-responsive">
          @if (count($cars) > 0)
          <table class="table table-striped mb-0">
            <thead>
              <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Merk</th>
                <th>Harga/hari</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($cars as $car)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$car->name}}</td>
                <td>{{$car->brand->name}}</td>
                <td>{{$car->getPrice()}}</td>
                <td>
                  <a href="{{route('cars.detail', [$car->id])}}" class="btn btn-secondary btn-action mr-1" data-toggle="tooltip" title="Detail"><i class="fas fa-eye"></i></a>
                  @if (!Auth::user()->isUser())
                  <a href="{{route('cars.edit', [$car->id])}}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                  <a href="{{route('cars.delete', [$car->id])}}" class="btn btn-danger btn-action btn-delete" data-toggle="tooltip" title="Delete"><i class="fas fa-trash"></i></a>
                  @else
                  <a href="{{route('cars.borrow', [$car->id])}}" class="btn btn-success btn-action" data-toggle="tooltip" title="Borrow"><i class="fas fa-car-side"></i></a>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <div class="col-md-12 text-center mt-5">
            <h3 class="title">Data Masih Kosong!</h3>
            <a href="{{route('cars.add')}}" class="btn btn-lg btn-success mt-2 mb-5"><span class="fas fa-plus"></span>&nbsp; Tambah</a>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@if (!Auth::user()->isUser())
@push('js')
<script>
  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        @foreach($months as $month)
          "{{$month}}",
        @endforeach
      ],
      datasets: [{
        label: 'Sales',
        data: [
          @foreach($stats as $stat)
            "{{$stat->sum}}",
          @endforeach
        ],
        borderWidth: 2,
        backgroundColor: 'rgba(63,82,227,.8)',
        borderWidth: 0,
        borderColor: 'transparent',
        pointBorderWidth: 0,
        pointRadius: 3.5,
        pointBackgroundColor: 'transparent',
        pointHoverBackgroundColor: 'rgba(63,82,227,.8)',
      }
      ]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          gridLines: {
            // display: false,
            drawBorder: false,
            color: '#f2f2f2',
          },
          ticks: {
            beginAtZero: true,
            stepSize: 1500000,
            callback: function (value, index, values) {
              return 'Rp.' + value;
            }
          }
        }],
        xAxes: [{
          gridLines: {
            display: false,
            tickMarkLength: 15,
          }
        }]
      },
    }
  });
  
</script>
@endpush
@endif