<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Notifications\NewUser;

Route::get('/', function () {
    return view('welcome');
    // return redirect()->route('login');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/terms-conditions', 'HomeController@terms')->name('terms');
Route::get('/read-notification/{id}', 'HomeController@readNotification')->name('read-notif');

// PROFILE
Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
    Route::get('/{user}', 'HomeController@profile')->name('index');
    Route::get('/edit/{user}', 'HomeController@changeProfile')->name('change');
});

// SETTINGS
Route::group(['prefix' => 'settings', 'as' => 'setting.'], function () {
    Route::get('/general', 'HomeController@general')->name('general');
    Route::get('/advance', 'HomeController@advance')->name('advance');
});

// CARS
Route::group(['prefix' => 'cars', 'as' => 'cars.'], function () {

    Route::get('/', 'CarController@index')->name('index');
    Route::get('/brands', 'BrandController@index')->name('brand');

    Route::get('/add', 'CarController@add')->name('add');
    Route::post('/new', 'CarController@create')->name('new');

    Route::get('/edit/{car}', 'CarController@edit')->name('edit');
    Route::post('/update', 'CarController@update')->name('update');

    Route::get('/delete/{car}', 'CarController@destroy')->name('delete');

    Route::get('/add/brand', 'BrandController@add')->name('add.brand');
    Route::post('/new/brand', 'BrandController@create')->name('new.brand');

    Route::get('/brand/edit/{brand}', 'BrandController@edit')->name('edit.brand');
    Route::post('/brand/update', 'BrandController@update')->name('update.brand');

    Route::get('/brand/delete/{brand}', 'BrandController@destroy')->name('delete.brand');

    Route::get('/detail/{car}/{notif_id?}', 'CarController@detail')->name('detail');

    Route::get('/search', 'CarController@search')->name('search');

    Route::get('/borrow/{car}', 'TransactionsController@borrow')->name('borrow');
    Route::post('/borrowing', 'TransactionsController@borrowing')->name('borrowing');
    Route::get('/borrowed/{type}', 'TransactionsController@borrowed')->name('borrowed');
});

// TRANSACTIONS
Route::group(['prefix' => 'transactions', 'as' => 'trans.'], function () {
    Route::get('/', 'TransactionsController@index')->name('index');
    Route::get('/borrowed/{type}', 'TransactionsController@borrowed')->name('borrowed');

    Route::get('/add/{trans}', 'TransactionsController@add')->name('add');
    Route::post('/new', 'TransactionsController@new')->name('new');

    Route::get('/edit/{trans}', 'TransactionsController@edit')->name('edit');
    Route::post('/update/{trans}', 'TransactionsController@update')->name('update');

    Route::get('/search', 'TransactionsController@search')->name('search');

    Route::get('/take/{trans}', 'TransactionsController@accept')->name('accept');

    Route::get('/delete/{trans}', 'TransactionsController@destroy')->name('delete');

    Route::get('/invoice/{trans}', 'TransactionsController@invoice')->name('invoice');
});

// USERS
Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
    Route::get('/', 'UsersController@index')->name('index');
    Route::get('/workers', 'UsersController@workers')->name('workers');

    Route::get('/profile/{user}', 'UsersController@profile')->name('profile');

    Route::get('/add/{user}', 'UsersController@add')->name('workers.add');
    Route::post('/new', 'UsersController@new')->name('workers.new');

    Route::get('/edit/{user}', 'UsersController@edit')->name('edit');
    Route::post('/update', 'UsersController@update')->name('update');

    Route::get('/search', 'CarController@search')->name('search');

    Route::get('/hire/{user}', 'UsersController@hire')->name('hire');
    Route::get('/kick/{user}', 'UsersController@kick')->name('kick');

    Route::get('/verif/{user}/{type}', 'UsersController@verif')->name('verif');
});


Route::get('/testnotif', function () {
    // Auth::user()->notify(new NewUser(User::find(1)));
    dd(substr(Auth::user()->notifications->first()->type, 18));
})->name('testnotif');
